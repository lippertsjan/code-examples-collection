use rand::thread_rng;
use rand::Rng;
 
fn main() {
    println!("Hello, world!");
    let mut rng = thread_rng();
    let _r1 = rng.gen::<i32>();
    let _r2 = rng.gen::<i32>();
    let _r3: u32 = rng.gen_range(0..10);
    let _r4: u32 = rng.gen_range(0..10);
}
